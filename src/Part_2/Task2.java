package Part_2;

public class Task2 {

    public static void main(String[] args) {
        String firstNumber = "56665770230948230";
        String secondNumber = "2342343244234";

        System.out.println(getSumOfNumbers(firstNumber, secondNumber));
    }

    public static String getReverseString(String value) {
        String result = new StringBuilder(value).reverse().toString();
        return result;
    }

    public static String addRemainingDigits(int lengthOfFirstNum, String second, String previousResult, int carry) {
        String currentResult = previousResult;
        int lengthOfSecond = second.length();

        for (int i = lengthOfFirstNum; i < lengthOfSecond; i++) {
            int currentDigit = (int) (second.charAt(i) - '0');
            int sum = currentDigit + carry;

            currentResult += (char) (sum % 10 + '0');

            carry = sum / 10;
        }

        if (carry > 0) {
            currentResult += (char) (carry + '0');
        }

        return currentResult;
    }

    public static String processedAdditionOfNumbers(String first, String second) {
        String result = "";
        int carry = 0;
        int lengthOfFirstNum = first.length();

        for (int i = 0; i < lengthOfFirstNum; i++) {
            int currentDigitOfFirstNum = (int) first.charAt(i) - '0';
            int currentDigitOfSecondNum = (int) second.charAt(i) - '0';

            int sum = currentDigitOfFirstNum + currentDigitOfSecondNum + carry;
            result += (char) (sum % 10 + '0');

            carry = sum / 10;
        }

        result = addRemainingDigits(lengthOfFirstNum, second, result, carry);

        return getReverseString(result);
    }

    public static String getSumOfNumbers(final String first, final String second) {
        Boolean isFirstNumberLarger = first.length() > second.length();

        String firstNumber = isFirstNumberLarger ? second : first;
        String secondNumber = isFirstNumberLarger ? first : second;

        String firstNumberReversed = getReverseString(firstNumber);
        String secondNumberReversed = getReverseString(secondNumber);

        String sumOfNumbers = processedAdditionOfNumbers(firstNumberReversed, secondNumberReversed);

        return sumOfNumbers;
    }

}
