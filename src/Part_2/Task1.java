package Part_2;

import static Part_1.Task1.getGreatestCommonDivisor;

public class Task1 {

    public static void main(String[] args) {
        System.out.println(checkThreeNumbersCoprime(2, 4, 13));
        System.out.println(checkThreeNumbersCoprime(2, 4, 12));
    }

    public static String checkThreeNumbersCoprime(double first, double second, double third) {
        Boolean areGivenNumbersCoprime = getGreatestCommonDivisor(getGreatestCommonDivisor(first, second), third) == 1.0;

        String result = first + " " + second + " " + third + ": ";
        result += areGivenNumbersCoprime ? "are coprime" : "are not coprime";

        return result;
    }

}
