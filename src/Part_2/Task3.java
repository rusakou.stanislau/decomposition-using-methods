package Part_2;

public class Task3 {

    public static void main(String[] args) {
        int greatestNumberOfScope = 200;
        String result = findArmstrongNumbers(greatestNumberOfScope);
        System.out.println("All Armstrong numbers from 1 to " + greatestNumberOfScope + ": " + result);
    }

    public static String findArmstrongNumbers(final int greatestNumber) {
        int currentNumber = 1;
        String armstrongNumbers = "";

        while (currentNumber <= greatestNumber) {
            boolean isCurrentNumberArmstrong = isNumberArmstrong(currentNumber);

            if (isCurrentNumberArmstrong) {
                armstrongNumbers += currentNumber + " ";
            }

            currentNumber++;
        }

        return armstrongNumbers;
    }

    public static boolean isNumberArmstrong(final int numberToCheck) {
        int numberTemp = numberToCheck;
        int cubeSum = 0;

        int amountDigitsInNumber = countDigitsInNumber(numberToCheck);

        while (numberTemp != 0) {
            int digit = numberTemp % 10;
            cubeSum += Math.pow(digit, amountDigitsInNumber);
            numberTemp /= 10;
        }

        return cubeSum == numberToCheck;
    }

    public static int countDigitsInNumber(int number) {
        String numberAsString = String.valueOf(number);

        int numberOfDigits = numberAsString.length();

        return numberOfDigits;
    }

}
