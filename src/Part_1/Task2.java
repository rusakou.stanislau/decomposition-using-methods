package Part_1;

public class Task2 {

    public static void main(String[] args) {
        double[] numbers = new double[]{12.0, 18.0, 60.0, 2.0};
        System.out.println("Finding the greatest common divisor of four natural numbers: " + getGreatestCommonDivisorOfFourNumbers(numbers));
    }

    public static Double getGreatestCommonDivisorOfFourNumbers(double[] numbers) {

        if (numbers.length >= 4) {
            double first = numbers[0];
            double second = numbers[1];
            double third = numbers[2];
            double fourth = numbers[3];

            return Task1.getGreatestCommonDivisor(
                    Task1.getGreatestCommonDivisor(first, second),
                    Task1.getGreatestCommonDivisor(third, fourth)
            );
        }

        return null;
    }
}
