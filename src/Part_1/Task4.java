package Part_1;

public class Task4 {

    public static void main(String[] args) {
        System.out.println("Calculating the sum of factorials of all odd numbers from 1 to 9: " + calculateSumOfFactorialOfOddNumbers(9.0));
    }

    public static double calculateSumOfFactorialOfOddNumbers(final double greatestNumber) {
        double greatestNumberTemp = greatestNumber;
        double index = 1.0;
        double sum = 0;

        while (index <= greatestNumberTemp) {
            boolean isNumberOdd = isNumberOdd(index);

            if (isNumberOdd) {
                sum += getFactorial(index);
            }

            index++;
        }

        return sum;
    }

    public static double getFactorial(final double number) {
        double numberTemp = number;
        double result = 1.0;

        for (double i = 1.0; i <= numberTemp; i++) {
            result *= i;
        }

        return result;
    }

    public static boolean isNumberOdd (double number) {
        return number % 2.0 != 0;
    }

}
