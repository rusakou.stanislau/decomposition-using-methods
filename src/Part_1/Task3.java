package Part_1;

public class Task3 {

    public static void main(String[] args) {
        System.out.println("Finding the smallest common multiple of three natural numbers: " + getSmallestCommonMultipleOfThreeNumbers(3.0, 23.0, 17.0));
    }

    public static Double getSmallestCommonMultipleOfThreeNumbers(double first, double second, double third) {
        return Task1.getSmallestCommonMultiple( Task1.getSmallestCommonMultiple(first, second), third );
    }

}
