package Part_1;


public class Task5 {

    public static void main(String[] args) {
        double numbers[] = new double[]{44.0, 66.0, 99.0, 77.0, 33.0, 22.0, 55.0};
        System.out.println("Find the second-largest number in the array A[N]: " + getSecondLargestNumber(numbers));
    }

    public static double getSecondLargestNumber(double numbers[]) {
        double[] numbersSortDesc = sortArrayDescending(numbers);
        return numbersSortDesc[1];
    }

    public static double[] sortArrayDescending(double[] numbers) {
        int indexOfLarges;
        double temp;

        for (int i = 0; i < numbers.length - 1; i++) {
            indexOfLarges = i;

            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] > numbers[indexOfLarges]) {
                    indexOfLarges = j;
                }
            }

            temp = numbers[indexOfLarges];
            numbers[indexOfLarges] = numbers[i];
            numbers[i] = temp;
        }

        return numbers;
    }

}
