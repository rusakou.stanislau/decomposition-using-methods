package Part_1;

public class Task1 {

    public static void main(String[] args) {
        double first = 14.0;
        double second = 17.0;

        System.out.println("finding the greatest common divisor: " + getGreatestCommonDivisor(first, second));
        System.out.println("finding the smallest common multiple: " + getSmallestCommonMultiple(first, second));
    }

    public static double getSmallestCommonMultiple(final double first, final double second) {
        return first * second / getGreatestCommonDivisor(first, second);
    }
    public static double getGreatestCommonDivisor(final double first, final double second) {
        double tempFist = first;
        double tempSecond = second;

        while (tempFist != 0 && tempSecond != 0) {
            if (tempFist > tempSecond)
                tempFist = tempFist % tempSecond;
            else
                tempSecond = tempSecond % tempFist;
        }

        return tempFist + tempSecond;
    }
}
